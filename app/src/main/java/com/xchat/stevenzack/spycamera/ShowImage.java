package com.xchat.stevenzack.spycamera;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.io.File;

/**
 * Created by steven on 17-6-10.
 */

public class ShowImage extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_img_shower);
        final File file=new File(getIntent().getStringExtra("path"));
        if (file.exists()){
            Bitmap bm= BitmapFactory.decodeFile(file.getAbsolutePath());
            ((ImageView)findViewById(R.id.imgshower_iv)).setImageBitmap(bm);
        }
        ((ImageButton)findViewById(R.id.imgshower_back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        ((ImageButton)findViewById(R.id.imgshower_delete)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                file.delete();
                getSharedPreferences("settings", Context.MODE_PRIVATE).edit().putString("lastImg", Environment.getExternalStorageDirectory().getAbsolutePath()).commit();
                finish();
            }
        });
    }
}
